#!/bin/bash

if [ ! -d build ]; then
    CC=clang++ meson build
fi

ninja -C build && ./build/pi $@


# 円周率πを計算するプログラム
このレポジトリには、レポートに添付してあるファイルがおいてある。

## 前提条件
私はLinux(Ubuntu)環境で実行しているため他の環境では動くかわからない。

### Ubuntuでのパッケージ
```bash
$ apt install meson ninja-build clang
```

## 実行
必要なパッケージがインストールされていれば次の方法で実行できる
```bash
$ chmod +x ./run.sh
$ ./run.sh 10
# ビルド関連の出力
10 iterations.
Pi = 3.1315929035585537
```


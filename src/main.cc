#include<iostream>
#include<cmath>
#include<limits>

using namespace std;

typedef std::numeric_limits<double> dbl;

int main(int argc, char const *argv[])
{
    if (argc < 2) {
        cerr << "specify a number" << endl;
        return 1;
    }

    double pi = 0;

    unsigned int n_max = atoi(argv[1]);

    for (unsigned int n = 0; n < n_max; n++) {
        pi += pow(-1, n) / (2 * n + 1);
    }

    pi = 4 * pi;

    cout.precision(dbl::max_digits10);
    cout << n_max << " iterations." << endl;
    cout << "Pi = " << pi << endl;

    return 0;
}

